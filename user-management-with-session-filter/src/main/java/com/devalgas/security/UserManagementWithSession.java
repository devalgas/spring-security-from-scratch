package com.devalgas.security;

import com.devalgas.security.domain.User;
import com.devalgas.security.domain.security.Role;
import com.devalgas.security.domain.security.UserRole;
import com.devalgas.security.repository.RoleRepository;
import com.devalgas.security.repository.UserRepository;
import com.devalgas.security.service.UserService;
import com.devalgas.security.utility.SecurityUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class UserManagementWithSession implements CommandLineRunner {
    @Autowired
    private UserService userService;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(UserManagementWithSession.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        roleRepository.deleteAll();
        userRepository.deleteAll();
        User user1 = new User();
        user1.setFirstName("dev");
        user1.setLastName("kobbi");
        user1.setUsername("devalgas");
        user1.setPassword(SecurityUtility.passwordEncoder().encode("password"));
        user1.setEmail("kamgadevalgas@gmail.com");
        Set<UserRole> userRoles = new HashSet<>();
        Role role1 = new Role();
        role1.setName("ROLE_USER");
        userRoles.add(new UserRole(user1, role1));

        userService.createUser(user1, userRoles);
        userRoles.clear();
    }
}
