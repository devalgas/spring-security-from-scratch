package com.devalgas.security.service;

import com.devalgas.security.domain.User;
import com.devalgas.security.domain.security.UserRole;

import java.util.Set;

/**
 * @author devalgas kamga on 18/09/2020. 17:31
 */
public interface UserService {
    User createUser(User user, Set<UserRole> userRoles);

}
