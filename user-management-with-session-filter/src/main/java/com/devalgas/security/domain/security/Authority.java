package com.devalgas.security.domain.security;

import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;

/**
 * @author devalgas kamga on 17/09/2020. 16:00
 */
public class Authority implements GrantedAuthority, Serializable {
    private final static long serialVersionUID = 11L;

    private final String authority;

    public Authority(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return this.authority;
    }
}
