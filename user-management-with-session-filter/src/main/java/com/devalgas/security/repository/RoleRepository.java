package com.devalgas.security.repository;

import com.devalgas.security.domain.security.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author devalgas kamga on 18/09/2020. 17:28
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
}
